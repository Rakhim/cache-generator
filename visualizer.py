import numpy as np
import matplotlib.pyplot as plt
import json as js
from numpy import *

data = js.loads(open('./statistic.json', 'r').read())

xs = []
ys = []
for pair in data:
    xs.append(pair[0])
    ys.append(pair[1])

plt.xlim(-1, max(xs) + 1)
plt.ylim(0, 1)

plt.plot(xs, ys, 'go-')
plt.ylabel('Процент попадания')
plt.xlabel('Пространственная локальность')
plt.grid()
plt.show()