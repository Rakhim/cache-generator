package groupid.generator;

public class Request {
    private final Long address;
    private final boolean read;

    public Request(Long address, boolean read) {
        this.address = address;
        this.read = read;
    }

    public Long getAddress() {
        return address;
    }

    public boolean isRead() {
        return read;
    }
}
