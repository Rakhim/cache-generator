package groupid.generator;

import java.util.ArrayList;
import java.util.List;

/**
 * Генератор запросов.
 * Только для линейного кода.
 */
public class RequestGeneratorHelper {

    private RequestGeneratorHelper() {
    }

    /**
     * Сгенерировать список обращений на чтение заданной длины представляющих
     * собой линейный код.
     * @param length    -   длина кода
     * @return          -   список обращений.
     */
    public static List<Request> generate(int length) {
        final List<Request> list = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            list.add(new Request((long) i, true));
        }
        return list;
    }
}
