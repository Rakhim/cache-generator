package groupid;

import java.util.List;

public class Arguments {
    private int groupCount;
    private int groupSize;
    private int requestCount;
    private List<Integer> list;
    private int timeout;

    public void setGroupCount(int groupCount) {
        this.groupCount = groupCount;
    }

    public int getGroupCount() {
        return groupCount;
    }

    public void setGroupSize(int groupSize) {
        this.groupSize = groupSize;
    }

    public int getGroupSize() {
        return groupSize;
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
    }

    public int getRequestCount() {
        return requestCount;
    }

    public void setSpatialLocalities(List<Integer> list) {
        this.list = list;
    }

    public List<Integer> getSpatialLocalities() {
        return list;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getTimeout() {
        return timeout;
    }

    @Override
    public String toString() {
        return  "{" +
                "\n groupCount: " + groupCount +
                "\n groupSize:" + groupSize +
                "\n requestCount: " + requestCount +
                "\n spatial localities: " + list +
                "\n timeout: " + timeout +
                "\n}";
    }
}
