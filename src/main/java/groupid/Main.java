package groupid;

import groupid.cache.Cache;
import groupid.generator.Request;
import groupid.generator.RequestGeneratorHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Main {
    private static String DEFAULT_FILE_NAME = "statistic.json";

    private static void readLocality(Cache cache, int address, int length) {
        final int start = (address / length) * length;
        for (int i = start; i < address; i++) {
            cache.process(i, true);
        }

        final int end = start + length;
        for (int i = address + 1; i < end; i++) {
            cache.process(i, true);
        }
    }

    private static double readWithLocality(Cache cache, List<Request> requests, int localityLength) {
        cache.clear();
        int hits = 0;
        for (Request request : requests) {
            final int address = request.getAddress().intValue();
            final boolean hit = cache.process(address, request.isRead());
            if (hit) {
                hits++;
            }

            if (!hit) {
                readLocality(cache, address, localityLength);
            }
        }

        return ((double) hits) / requests.size();
    }

    private static Arguments getArgumnts(String[] args) {
        final Arguments arguments = new Arguments();
        arguments.setGroupCount(1);
        arguments.setGroupSize(2048);
        arguments.setRequestCount(2048);
        arguments.setSpatialLocalities(Arrays.asList(2, 4, 8, 16, 32, 64, 128, 256, 1024));
        arguments.setTimeout(0);
        return arguments;
    }

    public static void main(String[] args) {
        final Arguments arguments = getArgumnts(args);
        final Cache cache = Cache.newInstance(arguments.getGroupCount(), arguments.getGroupSize());
        cache.setTimeout(arguments.getTimeout());

        final List<Request> requests = RequestGeneratorHelper.generate(arguments.getRequestCount());
        final List<Pair> hits = new ArrayList<>();
        for (int locality : arguments.getSpatialLocalities()) {
            hits.add(new Pair(locality, readWithLocality(cache, requests, locality)));
        }

        consoleLog(arguments);
        save(DEFAULT_FILE_NAME, hits);
        consoleLog(hits);
        show();
    }

    private static void consoleLog(Object data) {
        System.out.println(data);
    }

    private static void show() {
        try {
            Runtime.getRuntime().exec("python  visualizer.py");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void save(String fileName, List<Pair> list) {
        try (OutputStream out = new FileOutputStream(new File(fileName))) {
            out.write(String.valueOf(list).getBytes());
            out.flush();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static class Pair {
        private final int left;
        private final double frequency;

        Pair(int left, double right) {
            this.left = left;
            this.frequency = right;
        }

        @Override
        public String toString() {
            return String.format(Locale.US, "[%d, %8.8f]", left, frequency);
        }
    }
}
