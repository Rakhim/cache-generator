package groupid.cache;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Кэш.
 * Используется алгоритм замещения NRU - Not Recently Used.
 */
public class Cache {
    private final int groupCount;
    private final int groupSize;
    private final List<Group> groups = new ArrayList<>();
    private int timeout;
    private int requests = 0;

    public static Cache newInstance(int groupCount, int groupSize) {
        if (groupCount < 1) {
            throw new IllegalArgumentException(String.format("Количество групп должно быть больше нуля: %d", groupCount));
        }

        if (groupSize < 1) {
            throw new IllegalArgumentException(String.format("Размер группы должен быть больше нуля: %d", groupSize));
        }

        return new Cache(groupCount, groupSize);
    }

    private Cache(int groupCount, int groupSize) {
        this.groupCount = groupCount;
        this.groupSize = groupSize;
        initGroups(groupCount);
    }

    public void setTimeout(int timeout) {
        if (timeout < 0) {
            throw new IllegalArgumentException("Таймер должен быть неотрицателен.");
        }

        this.timeout = timeout;
    }

    private void initGroups(int groupCount) {
        for (int i = 0; i < groupCount; i++) {
            groups.add(initGroup(groupSize));
        }
    }

    private Group initGroup(int groupSize) {
        return new Group(groupSize);
    }

    private void clearReferenceFlag() {
        for (Group group : groups) {
            group.clearReferenceFlag();
        }
    }

    private void timer() {
        requests++;
        /**
         * TimeOut - грубо: количество запросов, спустя которое необходимо занулять
         * флаг чтения - reference flag
         *
         * Таким образом мы будем знать, что записи у которых флаг чтения true
         * использовались недавно т.е. такие записи Recently Used
         * А все остальные - Not Recently Used
         */
        if (timeout != 0 && requests % timeout == 0) {
            clearReferenceFlag();
            requests = 0;
        }
    }

    /**
     * Эмулировать запрос.
     *
     * @param address -   виртуальный адрес
     * @param read    -   признак чтения
     * @return -   признак кэш-попадания/кэш-промаха
     */
    public boolean process(int address, boolean read) {
        timer();

        final Group group = groups.get(getGroupNumber(address));
        final Entry candidate = group.getCandidate(address);
        final Category category = candidate.getCategory(address);
        candidate.setPresent(true);
        candidate.setReferenced(true);
        candidate.setModified(!read);
        candidate.setTag(address);
        return Category.CACHE_HIT.equals(category);
    }

    private int getGroupNumber(int address) {
        return address & (groupCount - 1);
    }

    @Override
    public String toString() {
        return Arrays.toString(groups.toArray());
    }

    public void clear() {
        for (Group group : groups) {
            group.clear();
        }
    }
}
