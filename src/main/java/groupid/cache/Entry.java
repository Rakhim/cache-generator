package groupid.cache;

/**
 * Запись кэша.
 */
class Entry {
    private int tag;
    private boolean present;
    private boolean referenced;
    private boolean modified;

    /**
     * Тег - поле по которму идентифицируется запись.
     * Представляет собой полный виртуальный адрес.
     *
     * @return -   тэг
     */
    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public void setPresent(boolean present) {
        this.present = present;
    }

    public void setReferenced(boolean referenced) {
        this.referenced = referenced;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    /**
     *  Добавить запись кэша в соответствующую категорию: {@link Category}
     * @param tag   -   тег
     * @return      -   категория
     */
    public Category getCategory(int tag) {
        return present && getTag() == tag ?  Category.CACHE_HIT :
                    referenced && modified ? Category.REFERENCED_MODIFIED :
                                referenced ? Category.REFERENCED_NOT_MODIFIED :
                                  modified ? Category.NOT_REFERENCED_MODIFIED :
                                             Category.NOT_REFERENCED_NOT_MODIFIED;
    }

    @Override
    public String toString() {
        return String.format("{%5s, (%5b, %5b, %5b)}", tag, present, referenced, modified);
    }
}
