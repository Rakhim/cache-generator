package groupid.cache;

import java.util.*;

/**
 * Группа записе кэша.
 */
class Group {
    private List<Entry> entries = new ArrayList<>();

    /**
     * Создать группу.
     *
     * @param groupSize -   размер группы
     */
    public Group(int groupSize) {
        for (int i = 0; i < groupSize; i++) {
            entries.add(initEntry());
        }
    }

    private Entry initEntry() {
        return new Entry();
    }

    /**
     * Очистить флаг чтения у всей группы
     */
    public void clearReferenceFlag() {
        for (Entry entry : entries) {
            entry.setReferenced(false);
        }
    }

    private SortedMap<Integer, List<Entry>> initCategoryMap() {
        final SortedMap<Integer, List<Entry>> categories = new TreeMap<>();
        categories.put(Category.CACHE_HIT.getOrdinal(), new ArrayList<Entry>()); // Искусственная категория для случая кэш-попадания
        categories.put(Category.NOT_REFERENCED_NOT_MODIFIED.getOrdinal(), new ArrayList<Entry>());
        categories.put(Category.NOT_REFERENCED_MODIFIED.getOrdinal(), new ArrayList<Entry>());
        categories.put(Category.REFERENCED_NOT_MODIFIED.getOrdinal(), new ArrayList<Entry>());
        categories.put(Category.REFERENCED_MODIFIED.getOrdinal(), new ArrayList<Entry>());
        return categories;
    }

    private SortedMap<Integer, List<Entry>> getCategoryMap(int tag) {
        final SortedMap<Integer, List<Entry>> map = initCategoryMap();
        for (Entry entry : entries) {
            map.get(entry.getCategory(tag).getOrdinal()).add(entry);
        }
        return map;
    }

    /**
     * Получить запись кэша. (Рекоммендованную к перезаписи или ранее кэшированную)
     *
     * @param tag -   тег
     * @return -   запись
     */
    public Entry getCandidate(int tag) {
        final Collection<List<Entry>> categories = getCategoryMap(tag).values();
        for (List<Entry> category : categories) {
            if (!category.isEmpty()) {
                return category.get(0);
            }
        }

        throw new IllegalStateException("Ситуация кэш-промаха и отсутствия кандидатов некорректна!");
    }

    @Override
    public String toString() {
        return Arrays.toString(entries.toArray());
    }

    public void clear() {
        for (Entry entry : entries) {
            entry.setTag(0);
            entry.setReferenced(false);
            entry.setPresent(false);
            entry.setModified(false);
        }
    }
}
