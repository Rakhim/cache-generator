package groupid.cache;

public enum Category {
    /**
     * кэш-попадание
     */
    CACHE_HIT {
        @Override
        public int getOrdinal() {
            return 0;
        }
    },
    /**
     * не считаны, не модифицированы
     */
    NOT_REFERENCED_NOT_MODIFIED {
        @Override
        public int getOrdinal() {
            return 1;
        }
    },
    /**
     * не считаны, модифицированы
     */
    NOT_REFERENCED_MODIFIED {
        @Override
        public int getOrdinal() {
            return 2;
        }
    },
    /**
     * считаны, не модифицированы
     */
    REFERENCED_NOT_MODIFIED {
        @Override
        public int getOrdinal() {
            return 3;
        }
    },
    /**
     * считаны, модифицированы
     */
    REFERENCED_MODIFIED {
        @Override
        public int getOrdinal() {
            return 4;
        }
    };

    public abstract int getOrdinal();
}
