package groupid.cache;

import org.junit.Test;

import static org.junit.Assert.*;

public class CacheTest {

    @Test(expected = IllegalArgumentException.class)
    public void newInstance_wrong_group_count() {
        Cache.newInstance(0, 1);
        fail();
    }

    @Test(expected = IllegalArgumentException.class)
    public void newInstance_wrong_group_size() {
        Cache.newInstance(1, 0);
        fail();
    }

    @Test(expected = IllegalArgumentException.class)
    public void setTimeout_wrong() {
        Cache.newInstance(1, 1).setTimeout(-1);
        fail();
    }

    @Test()
    public void setTimeout() {
        Cache.newInstance(1, 1).setTimeout(7);
    }

    @Test
    public void process_without_timeout_only_writes() {
        final Cache cache = Cache.newInstance(1, 1);
        assertFalse(cache.process(1, false));
        assertTrue(cache.process(1, false));
        assertTrue(cache.process(1, false));
        assertFalse(cache.process(2, false));
        assertTrue(cache.process(2, false));
        assertTrue(cache.process(2, false));
    }

    @Test
    public void process_without_timeout_only_reads() {
        final Cache cache = Cache.newInstance(1, 1);
        assertFalse(cache.process(1, true));
        assertTrue(cache.process(1, true));
        assertTrue(cache.process(1, true));
        assertFalse(cache.process(2, true));
        assertTrue(cache.process(2, true));
        assertTrue(cache.process(2, true));
    }

    @Test
    public void process_without_timeout_mixed() {
        final Cache cache = Cache.newInstance(1, 1);
        assertFalse(cache.process(1, true));
        assertTrue(cache.process(1, false));
        assertTrue(cache.process(1, true));
        assertFalse(cache.process(2, false));
        assertTrue(cache.process(2, true));
        assertTrue(cache.process(2, false));
    }

    @Test
    public void process_without_timeout_only_cache_misses() {
        final Cache cache = Cache.newInstance(1, 1);
        for (int i = 0; i < 100; i++) {
            assertFalse(cache.process(i, Math.random() < 0.5));
        }
    }

    @Test
    public void process_with_timeout() {
        final Cache cache = Cache.newInstance(1, 1);
        cache.setTimeout(4);
        assertFalse(cache.process(0, true));
        assertFalse(cache.process(1, false));
        assertFalse(cache.process(2, false));
        assertFalse(cache.process(3, true));
        assertFalse(cache.process(4, true));
        assertFalse(cache.process(5, true));
    }
}