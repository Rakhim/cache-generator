package groupid.cache;

import org.junit.Test;

import static org.junit.Assert.*;

public class EntryTest {

    private Category getCategory(boolean referenced, boolean modified) {
        final Entry entry = new Entry();
        entry.setReferenced(referenced);
        entry.setModified(modified);
        return entry.getCategory(0);
    }

    @Test
    public void getCategory() {
        assertEquals(Category.NOT_REFERENCED_NOT_MODIFIED, getCategory(false, false));
        assertEquals(Category.NOT_REFERENCED_MODIFIED, getCategory(false, true));
        assertEquals(Category.REFERENCED_NOT_MODIFIED, getCategory(true, false));
        assertEquals(Category.REFERENCED_MODIFIED, getCategory(true, true));
    }
}